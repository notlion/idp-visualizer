#include "cinder/CameraUi.h"
#include "cinder/Json.h"
#include "cinder/Rand.h"
#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/audio/audio.h"
#include "cinder/gl/gl.h"

#include "Connexion.h"
#include "PerspWarp.hpp"
#include "Utils.hpp"

using namespace ci;
using namespace ci::app;

constexpr int IDP_FFT_SIZE = 512;
constexpr int IDP_MEL_SIZE = 512;
constexpr int IDP_FFT_WINDOW = 1024;
constexpr int IDP_FFT_HISTORY = 128;
constexpr int IDP_LISSAJOUS_SIZE = 256;

const char *spectrumVertSrc = R"(
precision highp float;

uniform mat4 ciModelViewProjection;

in vec4 ciPosition;
out float vOpacity;

void main() {
    vOpacity = clamp(ciPosition.y * 20.0, 0.0, 1.0);
    gl_Position = ciModelViewProjection * ciPosition;
}
)";

const char *spectrumFragSrc = R"(
precision highp float;

uniform vec4 ciColor;

in float vOpacity;
out vec4 fragColor;

void main() {
    fragColor = vec4(0.25) * vOpacity;
}
)";

class IdpApp : public App {
    audio::InputDeviceNodeRef mInputDeviceNode;
    audio::MonitorNodeRef mMonitorNode;
    audio::MonitorSpectralNodeRef mMonitorSpectralNode;

    std::vector<vec3> mSpectrumPositions;
    gl::BatchRef mSpectrumBatch;

    std::vector<vec3> mLissajousPositions;
    gl::BatchRef mLissajousBatch;
    bool mShowLissajous = true;

    gl::FboRef mRenderFbo;

    CameraPersp mCamera;
    CameraUi mCameraUI;

    vec3 mSpectrumScale = vec3(1.0f);
    vec3 mLissajousScale = vec3(0.5f);
    vec2 mLissajousPosition = vec2(0.85f, 0.5f);

    bool mPaused = false;

    float *mSpectrumBuffer;
    float *mMelBuffer;
    float *mMelWeights;

    float *mTerhardt;

    //
    // Perspective Warp Stuff
    //

    //! TRUE if the transform matrix needs to be recalculated
    bool mIsInvalid = true;

    //! size in pixels of the actual content
    float mWidth;
    float mHeight;

    //! corners expressed in content coordinates
    vec2 mSource[4];
    //! corners expressed in window coordinates
    vec2 mDestination[4];
    //! corners expressed in normalized window coordinates
    vec2 mDestinationNormalized[4];

    //! warp transform matrix
    mat4 mTransform;

    //! edit variables
    bool mIsEditingWarp = false;
    bool mIsMouseDown = false;

    size_t mSelected = 0;

    ivec2 mInitialMouse;
    ivec2 mCurrentMouse;

    vec2 mInitialPosition;

    //! set the size in pixels of the actual content
    void setContentSize(float width, float height);

    //! find the index of the nearest corner
    size_t getNearestIndex(const ivec2 &pt) const;

  public:
    void setup() override;
    void update() override;
    void draw() override;

    void resize() override;

    void mouseDown(MouseEvent event) override;
    void mouseDrag(MouseEvent event) override;
    void mouseUp(MouseEvent event) override;

    void keyDown(KeyEvent event) override;

    void saveSettings();
    void loadSettings();
};

void IdpApp::setup() {
    {
        auto ctx = audio::Context::master();

        // The InputDeviceNode is platform-specific, so you create it using a special method on the Context:
        mInputDeviceNode = ctx->createInputDeviceNode();

        getWindow()->setTitle(mInputDeviceNode->getDevice()->getName());

        {
            // By providing an FFT size double that of the window size, we 'zero-pad' the analysis data, which gives
            // an increase in resolution of the resulting spectrum data.
            auto fmt = audio::MonitorSpectralNode::Format().fftSize(IDP_FFT_SIZE).windowSize(IDP_FFT_WINDOW);
            mMonitorSpectralNode = ctx->makeNode(new audio::MonitorSpectralNode(fmt));
            mInputDeviceNode >> mMonitorSpectralNode;
        }

        {
            auto fmt = audio::MonitorNode::Format().windowSize(IDP_LISSAJOUS_SIZE * 2);
            mMonitorNode = ctx->makeNode(new audio::MonitorNode(fmt));
            mInputDeviceNode >> mMonitorNode;
        }

        // InputDeviceNode (and all InputNode subclasses) need to be enabled()'s to process audio. So does the Context:
        mInputDeviceNode->enable();
        ctx->enable();
    }

    {
        // Init FFT

        const auto vertexCount = IDP_MEL_SIZE * IDP_FFT_HISTORY;
        mSpectrumPositions.resize(vertexCount);

        std::vector<uint32_t> indices((vertexCount - IDP_MEL_SIZE) * 2);
        for (uint32_t i = 0; i < indices.size(); ++i) {
            indices[i] = (i / 2) + (IDP_MEL_SIZE * (i % 2));
        }

        const auto indicesVbo = gl::Vbo::create(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

        const auto mesh = gl::VboMesh::create(mSpectrumPositions.size(),
                                              GL_LINES,
                                              { gl::VboMesh::Layout().attrib(geom::AttribInfo(geom::Attrib::POSITION, 3, 0, 0)) },
                                              indices.size(),
                                              GL_UNSIGNED_INT,
                                              indicesVbo);
        mesh->bufferAttrib(geom::Attrib::POSITION, mSpectrumPositions);

        // const auto prog = gl::getStockShader(gl::ShaderDef().color());
        const auto prog = gl::GlslProg::create(gl::GlslProg::Format().vertex(spectrumVertSrc).fragment(spectrumFragSrc));
        mSpectrumBatch = gl::Batch::create(mesh, prog);
    }

    {
        // Init Lissajous

        mLissajousPositions.resize(IDP_LISSAJOUS_SIZE);

        std::vector<uint32_t> indices((IDP_LISSAJOUS_SIZE - 1) * 2);
        for (uint32_t i = 0; i < IDP_LISSAJOUS_SIZE - 1; ++i) {
            indices[i * 2] = i;
            indices[i * 2 + 1] = i + 1;
        }

        const auto indicesVbo = gl::Vbo::create(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

        const auto mesh = gl::VboMesh::create(mLissajousPositions.size(),
                                              GL_LINES,
                                              { gl::VboMesh::Layout().attrib(geom::AttribInfo(geom::Attrib::POSITION, 3, 0, 0)) },
                                              indices.size(),
                                              GL_UNSIGNED_INT,
                                              indicesVbo);
        mesh->bufferAttrib(geom::Attrib::POSITION, mLissajousPositions);

        const auto prog = gl::getStockShader(gl::ShaderDef().color());
        mLissajousBatch = gl::Batch::create(mesh, prog);
    }

    {
        // Init Camera

        mCamera.setFov(100);
        mCamera.lookAt(normalize(vec3(0.0f, 2.0f, 2.0f)) * 2.0f, vec3(0.0f));
        mCameraUI = CameraUi(&mCamera);

        if (!mIsEditingWarp) {
            mCameraUI.connect(getWindow());
        }
    }

    {
        // Init Space Navigator

        reza::con::Connexion::getSignalUpdate().connect([this](reza::con::ConnexionData data) {
            auto trans = data.getTranslation();
            auto translation = glm::rotate(mCamera.getOrientation(), glm::vec3(trans.x, -trans.z, trans.y)) * 0.0005f;
            auto euler = data.getRotation();
            auto rotation = glm::quat(glm::vec3(euler.x, -euler.z, euler.y) * 0.00025f);

            mCamera.setEyePoint(mCamera.getEyePoint() + translation);
            mCamera.setOrientation(mCamera.getOrientation() * rotation);
        });
        reza::con::Connexion::start();
    }

    {
        // mark stuff

        const float minMel = hzToMel(20.f);
        const float maxMel = hzToMel(11025.f);
        float melFreqs[IDP_MEL_SIZE + 2];
        for (unsigned int i = 0; i < IDP_MEL_SIZE + 2; i++) {
            float f = (float)i / (IDP_MEL_SIZE + 1);
            float mel = f * (maxMel - minMel) + minMel;
            melFreqs[i] = melToHz(mel);
        }

        size_t sampleRate = mInputDeviceNode->getSampleRate();
        float fftFreqs[IDP_FFT_SIZE];
        for (unsigned int i = 0; i < IDP_FFT_SIZE; i++) {
            float f = (float)i / IDP_FFT_SIZE;
            fftFreqs[i] = f * sampleRate / 2.f;
        }

        mMelWeights = (float *)malloc(IDP_MEL_SIZE * IDP_FFT_SIZE * sizeof(float *));
        memset(mMelWeights, 0, IDP_MEL_SIZE * sizeof(float *));
        for (int i = 0; i < IDP_MEL_SIZE; i++) {
            float melNorm = 2.f / (melFreqs[i + 2] - melFreqs[i]);
            for (int j = 0; j < IDP_FFT_SIZE; j++) {
                float lower = (fftFreqs[j] - melFreqs[i]) / (melFreqs[i + 1] - melFreqs[i]);
                float upper = (melFreqs[i + 2] - fftFreqs[j]) / (melFreqs[i + 2] - melFreqs[i + 1]);

                mMelWeights[i * IDP_FFT_SIZE + j] = fmaxf(0.f, fminf(lower, upper)) * melNorm;
                //printf("%f\n", _melWeights[j*numMel+i]);
            }
            //puts("--------");
        }

        mTerhardt = (float *)malloc(IDP_FFT_SIZE * sizeof(float));
        float hzPerBin = sampleRate / (IDP_FFT_SIZE * 2);
        for (int i = 0; i < IDP_FFT_SIZE; i++) {
            float f = hzPerBin * i / 1000.f;
            mTerhardt[i] = powf(10.f, (-3.64f * powf(f, -0.8f) + 6.5f * expf(-0.6f * powf(f - 3.3f, 2.f)) - 0.001f * powf(f, 4.f)) / 10.f);
        }

        mSpectrumBuffer = (float *)malloc(IDP_FFT_SIZE * sizeof(float));
        mMelBuffer = (float *)malloc(IDP_MEL_SIZE * sizeof(float));
    }

    {
        // Perspective Warp

        // determine where the four corners should be,
        // expressed in normalized window coordinates
        mDestinationNormalized[0].x = 0.0f;
        mDestinationNormalized[0].y = 0.0f;
        mDestinationNormalized[1].x = 1.0f;
        mDestinationNormalized[1].y = 0.0f;
        mDestinationNormalized[2].x = 1.0f;
        mDestinationNormalized[2].y = 1.0f;
        mDestinationNormalized[3].x = 0.0f;
        mDestinationNormalized[3].y = 1.0f;
    }

    loadSettings();
}

void IdpApp::resize() {
    setContentSize(getWindowWidth(), getWindowHeight());
}

void IdpApp::update() {
    if (!mPaused) {
        {
            // Update FFT

            const auto &magSpectrum = mMonitorSpectralNode->getMagSpectrum();

            std::rotate(mSpectrumPositions.rbegin(), mSpectrumPositions.rbegin() + IDP_MEL_SIZE, mSpectrumPositions.rend());

            for (size_t i = 0; i < IDP_FFT_SIZE; ++i) {
                mSpectrumBuffer[i] = magSpectrum[i];
            }

            // apply perceptual loudness
            vDSP_vmul(mSpectrumBuffer, 1, mTerhardt, 1, mSpectrumBuffer, 1, IDP_FFT_SIZE);

            // spread
            vDSP_mmul(mMelWeights, 1, mSpectrumBuffer, 1, mMelBuffer, 1, IDP_MEL_SIZE, 1, IDP_FFT_SIZE);

            // debug!
            // for (size_t i = 0; i < mNumMel; ++i) {
            //     //mMelBuffer[i] = magSpectrum[i];
            //     mMelBuffer[i] = audio::linearToDecibel(magSpectrum[i]) / 100.0f;
            // }

            const auto spectrumScaleZ = 8.0f;
            const auto spectrumStepZ = 2.0f * spectrumScaleZ / IDP_FFT_HISTORY;

            for (auto &pos : mSpectrumPositions) {
                pos.z -= spectrumStepZ;
            }
            for (size_t i = 0; i < IDP_MEL_SIZE; ++i) {
                mSpectrumPositions[i].x = mix(-1.0f, 1.0f, float(i) / float(IDP_MEL_SIZE - 1));
                mSpectrumPositions[i].y = audio::linearToDecibel(mMelBuffer[i]) / 100.0f;
                mSpectrumPositions[i].z = spectrumScaleZ;
            }
            mSpectrumBatch->getVboMesh()->bufferAttrib(geom::Attrib::POSITION, mSpectrumPositions);
        }

        {
            // Update Lissajous

            const auto &buffer = mMonitorNode->getBuffer();

            assert(mLissajousPositions.size() * 2 <= buffer.getNumFrames());

            // x = buffer[i] * g_lissajous_scale;
            // if( channels == 1 )
            // {
            //     // delay
            //     y = (i - g_delay >= 0) ? buffer[i-g_delay] : g_back_buffer[len + i-g_delay];
            //     y *= g_lissajous_scale;
            // }
            // else
            // {
            //     y = buffer[i + channels-1] * g_lissajous_scale;
            // }

            if (buffer.getNumChannels() == 1) {
                for (size_t i = 0; i < mLissajousPositions.size(); ++i) {
                    mLissajousPositions[i].x = *(buffer.getChannel(0) + i);
                    mLissajousPositions[i].y = *(buffer.getChannel(0) + i + mLissajousPositions.size());
                    mLissajousPositions[i].z = 0.0f;
                }
            }
            else {
                for (size_t i = 0; i < mLissajousPositions.size(); ++i) {
                    mLissajousPositions[i].x = *(buffer.getChannel(0) + i);
                    mLissajousPositions[i].y = *(buffer.getChannel(1) + i + mLissajousPositions.size());
                    mLissajousPositions[i].z = 0.0f;
                }
            }

            mLissajousBatch->getVboMesh()->bufferAttrib(geom::Attrib::POSITION, mLissajousPositions);
        }
    }

    {
        // Perspective Warp

        if (mIsInvalid) {
            const vec2 windowSize = getWindowSize();

            for (size_t i = 0; i < 4; ++i) {
                mDestination[i] = windowSize * mDestinationNormalized[i];
            }

            mTransform = getPerspectiveTransform(mSource, mDestination);

            mIsInvalid = false;
        }
    }
}

void IdpApp::draw() {
    {
        if (!mRenderFbo || mRenderFbo->getSize() != getWindowSize()) {
            auto fmt = gl::Fbo::Format().colorTexture(gl::Texture::Format()
                                                              .magFilter(GL_LINEAR)
                                                              .minFilter(GL_LINEAR_MIPMAP_LINEAR)
                                                              .mipmap())
                               .samples(8);
            mRenderFbo = gl::Fbo::create(getWindowWidth(), getWindowHeight(), fmt);
        }

        gl::ScopedFramebuffer scopedFbo(mRenderFbo);
        gl::clear();

        {
            gl::color(ColorAf(vec4(0.333f)));
            gl::enableAlphaBlendingPremult();

            mCamera.setAspectRatio(getWindowAspectRatio());
            gl::ScopedMatrices scopedMats;
            gl::setMatrices(mCamera);
            gl::scale(mSpectrumScale);

            mSpectrumBatch->draw();
        }

        if (mShowLissajous) {
            gl::color(ColorAf(vec4(0.5f)));
            gl::enableAlphaBlendingPremult();

            const auto windowSize = getWindowSize();

            gl::ScopedMatrices scopedMats;
            gl::setMatricesWindow(windowSize);
            gl::translate(vec2(windowSize) * mLissajousPosition);
            gl::scale(mLissajousScale * float(windowSize.y));

            mLissajousBatch->draw();
        }
    }

    {
        gl::clear();

        gl::ScopedTextureBind scopedTex(mRenderFbo->getColorTexture());
        gl::ScopedModelMatrix scopedModel;
        gl::multModelMatrix(mTransform);

        gl::color(Color::white());
        gl::draw(mRenderFbo->getColorTexture());

        if (mIsEditingWarp) {
            gl::drawStrokedRect(Rectf(0, 0, mWidth, mHeight));
            gl::drawLine(vec2(0, 0), vec2(mWidth, mHeight));
            gl::drawLine(vec2(0, mHeight), vec2(mWidth, 0));
        }
    }
}

void IdpApp::setContentSize(float width, float height) {
    //! width and height of the CONTENT of your warp (desired resolution)
    mWidth = width;
    mHeight = height;

    //! set the four corners of the CONTENT
    mSource[0].x = 0.0f;
    mSource[0].y = 0.0f;
    mSource[1].x = mWidth;
    mSource[1].y = 0.0f;
    mSource[2].x = mWidth;
    mSource[2].y = mHeight;
    mSource[3].x = 0.0f;
    mSource[3].y = mHeight;

    //! we need to recalculate the warp transform
    mIsInvalid = true;
}

size_t IdpApp::getNearestIndex(const ivec2 &pt) const {
    uint8_t index = 0;
    float distance = 10.0e6f;

    for (uint8_t i = 0; i < 4; ++i) {
        const float d = glm::distance(vec2(mDestination[i].x, mDestination[i].y), vec2(pt));
        if (d < distance) {
            distance = d;
            index = i;
        }
    }

    return index;
}

void IdpApp::saveSettings() {
    auto warpJson = JsonTree::makeArray("warp");
    for (const auto &point : mDestinationNormalized) {
        warpJson.pushBack(JsonTree("", point.x));
        warpJson.pushBack(JsonTree("", point.y));
    }

    auto settingsJson = JsonTree::makeObject("settings");
    settingsJson.addChild(warpJson);

    settingsJson.write(getAppPath() / "settings.json");
}

void IdpApp::loadSettings() {
    const auto settingsPath = getAppPath() / "settings.json";
    if (fs::exists(settingsPath)) {
        try {
            const auto settingsJson = JsonTree(loadFile(settingsPath));
            const auto warpJson = settingsJson["warp"];
            for (size_t i = 0; i < 4; ++i) {
                mDestinationNormalized[i].x = warpJson.getValueAtIndex<float>(i * 2);
                mDestinationNormalized[i].y = warpJson.getValueAtIndex<float>(i * 2 + 1);
                mDestination[i] = mDestinationNormalized[i] * vec2(getWindowSize());
                mIsInvalid = true;
            }
        }
        catch (...) {
        }
    }
}

void IdpApp::mouseDown(MouseEvent event) {
    if (mIsEditingWarp) {
        // start dragging the nearest corner
        mIsMouseDown = true;
        mInitialMouse = mCurrentMouse = event.getPos();

        mSelected = getNearestIndex(mInitialMouse);
        mInitialPosition = mDestination[mSelected];
    }
}

void IdpApp::mouseDrag(MouseEvent event) {
    if (mIsEditingWarp) {
        // drag the nearest corner
        mCurrentMouse = event.getPos();

        const ivec2 d = mCurrentMouse - mInitialMouse;
        mDestination[mSelected].x = mInitialPosition.x + d.x;
        mDestination[mSelected].y = mInitialPosition.y + d.y;

        // don't forget to also update the normalized destination
        mDestinationNormalized[mSelected].x = mDestination[mSelected].x / getWindowWidth();
        mDestinationNormalized[mSelected].y = mDestination[mSelected].y / getWindowHeight();

        // recalculate transform matrix
        mIsInvalid = true;
    }
}

void IdpApp::mouseUp(MouseEvent event) {
    if (mIsEditingWarp) {
        // stop dragging the nearest corner
        mIsMouseDown = false;
    }
}

void IdpApp::keyDown(KeyEvent event) {
    switch (event.getCode()) {
        case KeyEvent::KEY_SPACE: {
            mPaused = !mPaused;
            break;
        }
        case KeyEvent::KEY_s: {
            saveSettings();
            break;
        }
        case KeyEvent::KEY_w: {
            mIsMouseDown = false;
            mIsEditingWarp = !mIsEditingWarp;
            if (mIsEditingWarp) {
                mCameraUI.disconnect();
            }
            else {
                mCameraUI.connect(getWindow());
            }
            break;
        }
        case KeyEvent::KEY_x: {
            mSpectrumScale.x *= -1.0f;
            break;
        }
        case KeyEvent::KEY_y: {
            mSpectrumScale.y *= -1.0f;
            break;
        }
        case KeyEvent::KEY_z: {
            mSpectrumScale.z *= -1.0f;
            break;
        }
        case KeyEvent::KEY_UP: {
            mLissajousPosition.y -= 0.01f;
            break;
        }
        case KeyEvent::KEY_DOWN: {
            mLissajousPosition.y += 0.01f;
            break;
        }
        case KeyEvent::KEY_LEFT: {
            mLissajousPosition.x -= 0.01f;
            break;
        }
        case KeyEvent::KEY_RIGHT: {
            mLissajousPosition.x += 0.01f;
            break;
        }
        case KeyEvent::KEY_l: {
            mShowLissajous = !mShowLissajous;
            break;
        }
    }
}

static void prepareSettings(App::Settings *settings) {
    settings->setWindowSize(1280, 720);
}

CINDER_APP(IdpApp, RendererGl(RendererGl::Options().msaa(4)), prepareSettings)
