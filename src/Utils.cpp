#include "Utils.hpp"

#include <cmath>

float hzToMel(float hz) {
    static const float fSp = 66.667f;

    static const float minLogHz = 1000.f;
    if (hz < minLogHz) {
        return hz / fSp;
    }

    static const float logStep = 0.068751777420949123f; //logf(6.4f) / 27.f;
    const float minLogMel = minLogHz / fSp;
    return minLogMel + std::logf(hz / minLogHz) / logStep;
}

float melToHz(float mel) {
    static const float fSp = 66.667f;
    static const float minLogHz = 1000.f;
    const float minLogMel = minLogHz / fSp;
    if (mel < minLogMel) {
        return fSp * mel;
    }

    static const float logStep = 0.068751777420949123f; //logf(6.4f) / 27.f;
    return minLogHz * std::expf(logStep * (mel - minLogMel));
}
