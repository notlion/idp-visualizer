#pragma once

#include "cinder/Vector.h"
#include "cinder/Matrix.h"

ci::mat4 getPerspectiveTransform(const ci::vec2 src[4], const ci::vec2 dst[4]);
