# Visualizer for IDP

## Usage

IDP Visualizer will choose the active audio input when the app is launched. To change the audio input while the app is running, close the app and reopen.

3DConnexion SpaceNavigator can be used for camera movement.

Use the built-in fullscreen on macOS.

### Keys

(space) → Toggle Pause

S → Save Settings (Settings are loaded on app startup)

W → Toggle Warp Mode (Drag corners of the X to adjust warping)

[X, Y, Z] → Flip Axis

L → Toggle Lissajous Visibility

(arrow keys) → Move Lissajous
